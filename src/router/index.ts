import Vue from 'vue'
import Router from 'vue-router'
import Admin from './../components/admin-container.vue'
import Canvas from './../components/canvas-container.vue'
import Login from './../components/login-container.vue'

Vue.use(Router)

export default new Router({
    routes: [
      { path: '/', component: Login },
      { path: '/admin/:name', component: Admin },
      { path: '/canvas/:id/:name', component: Canvas },
      { path: '*', redirect: '/' }

    ]
})